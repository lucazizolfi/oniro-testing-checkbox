unit: job
plugin: shell
category_id: GPU
id: graphics/{index}_auto_switch_card_{product_slug}
_summary: HW_GPU_001 - Switch GPU to {vendor} {product} and reboot
_description:
 Switch GPU to {vendor} {product} and reboot the machine
user: root
command:
 {switch_to_cmd}
 pm_test.py --silent --checkbox-respawn-cmd "$PLAINBOX_SESSION_SHARE"/__respawn_checkbox reboot --log-level=debug --log-dir="$PLAINBOX_SESSION_SHARE"


plugin: shell
category_id: GPU
id: graphics/VESA_drivers_not_in_use
command: cat /var/log/Xorg.0.log ~/.local/share/xorg/Xorg.0.log 2>&1 | perl -e '$a=0;while(<>){$a++ if /Loading.*vesa_drv\.so/;$a-- if /Unloading.*vesa/&&$a}exit 1 if $a'
_description: Check that VESA drivers are not in use
_summary: HW_GPU_002 - Check that VESA drivers are not in use


plugin: shell
category_id: GPU
id: graphics/{index}_driver_version_{product_slug}
command:
 # shellcheck disable=SC1091
 source graphics_env.sh {driver} {index}
 if [[ $XDG_SESSION_TYPE == "wayland" ]]
 then
   inxi_snapshot -Gazy
 else
   graphics_driver.py
 fi
_description: Parses Xorg.0.log and discovers the running X driver and version for the {vendor} {product} graphics card
_summary: HW_GPU_003 - Test X driver/version for {vendor} {product}


plugin: shell
template-resource: graphics_card
category_id: GPU
id: graphics/{index}_gl_support_{product_slug}
command:
  "$CHECKBOX_RUNTIME"/usr/lib/nux/unity_support_test -p 2>&1
_description: Check that {vendor} {product} hardware is able to run a desktop session (OpenGL)
_summary: HW_GPU_004 - Test OpenGL support for {vendor} {product}
user: root


plugin: shell
category_id: GPU
id: graphics/{index}_minimum_resolution_{product_slug}
command:
 # shellcheck disable=SC1091
 source graphics_env.sh {driver} {index}
 resolution_test.py --horizontal 800 --vertical 600
_summary: HW_GPU_005 - Test that {vendor} {product} meets minimum resolution requirement
_description:
 Ensure the current resolution meets or exceeds the recommended minimum
 resolution (800x600) on the {vendor} {product} graphics card. See here for details:
 https://help.ubuntu.com/community/Installation/SystemRequirements


plugin: shell
category_id: GPU
id: suspend/{index}_resolution_before_suspend_{product_slug}_auto
after: graphics/{index}_auto_switch_card_{product_slug}
_summary: HW_GPU_006 - Record the current resolution before suspending.
_description: Record the current resolution before suspending.
command:
 # shellcheck disable=SC1091
 source graphics_env.sh {driver} {index}
 xrandr -q | grep "[*]" | awk '{{print $1}}' > "$PLAINBOX_SESSION_SHARE"/{index}_resolution_before_suspend.txt